account assets:SPI
account assets:debian-ch
account assets:debian-france
account assets:stripe
account assets:cash
account assets:doubleo

account expenses:accommodation:dormitory:bed-set
account expenses:accommodation:dormitory:debcamp
account expenses:accommodation:dormitory:debconf
account expenses:accommodation:guesthouse
account expenses:equipment:av-and-network
account expenses:fees:bank
account expenses:fees:import-tax
account expenses:fees:SPI
account expenses:food:bar
account expenses:food:debcamp
account expenses:food:debconf
account expenses:incidental
account expenses:insurance
account expenses:local-stuff
account expenses:local-team
account expenses:printing
account expenses:site-visit
account expenses:social:cheese-and-wine
account expenses:social:conference-dinner
account expenses:social:day-trip
account expenses:swag
account expenses:travel:bursary:diversity
account expenses:travel:bursary:general
account expenses:travel:team:dc25
account expenses:travel:team:debconf-ctte
account expenses:travel:team:registration
account expenses:travel:team:video
account expenses:venue:covid
account expenses:venue:rooms
account expenses:visa

account income:accommodation
account income:bar
account income:meal
account income:registration
account income:sponsors:platinum
account income:sponsors:gold
account income:sponsors:silver
account income:sponsors:bronze
account income:sponsors:supporter
account income:sponsors:donations

; For sponsors that commit to reimbursment
account receivable:sponsors

; Create liabilility accounts for orga incurring expenses here
account liabilities:cwryu
account liabilities:gybang
account liabilities:jmkim
account liabilities:kwabang
account liabilities:nattie
account liabilities:roul
account liabilities:youngbin
